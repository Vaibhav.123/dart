/*
	         *
               * *
             * * *
               * *
                 *

*/
import 'dart:io';
void main(){
	
	print("Enter rows:");
	int rows = int.parse(stdin.readLineSync()!);
	int temp = rows;
	int x = 0;
	for(int i=1; i<=(rows*2)-1; i++){
		
		if(i<=rows){
			x=i;	
		}else{
			x=i-rows;
		}

		for(int j=1; j<=rows; j++){
			if(j<=x){
				stdout.write(" ");
			}else
				stdout.write("*");
		}
		print("");
	}
}
