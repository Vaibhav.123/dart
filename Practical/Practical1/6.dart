/* 
 	 *       *
           *   *
             *
           *   *
         *       *

*/

import 'dart:io';
void main(){
	
	print("Enter rows");
	int rows = int.parse(stdin.readLineSync()!);
	int j=1;
	for(int i=1; i<=(rows*2)-1; i++){
		if(i<=j || (i+j == rows+1))
			stdout.write("*");
		else
			print(" ");
		if(j==rows+2){
			print("");
		}			
		j++;
	}
}
