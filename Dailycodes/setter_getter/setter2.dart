import 'setter1.dart';
void main(){
	Company obj = new Company(10,"Sagar",3.5);
	obj.disp();
	// way1 
	obj.setId(12);
	
	// way2
	obj.setName = "Vaibhav";

	// way3
	obj.setSal = 2.5;

	obj.disp();
}
