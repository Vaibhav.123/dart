
class Employee {
	int? _id;
	String? _str;
	double? _sal;
	Employee (this._id,this._str,this._sal);
	
	// way 1 =>
	int? getId(){
		return _id;
	}
	
	// way 2 =>
	String? get getName{
		return _str;
	}

	// way 3 =>
	get getSal => _sal;
}
