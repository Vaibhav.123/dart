
class Company {
	int? _id;
	String? _str;
	double? _sal;
	Company (this._id,this._str,this._sal);
	
	// way 1 =>
	void setId(int id){
		_id = id;
	}
	
	// way 2 =>
	void set setName(String name){
		_str = name;
	}

	// way 3 =>
	set setSal(double sal) => _sal=sal;

	void disp(){
		print(_id);
		print(_str);
		print(_sal);
	}
}
