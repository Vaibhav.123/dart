

import 'dart:io';

class Employee {
	
	String? name;
	int? id;
	double? sal;

	void EmpInfo(){
		
		print("Name : $name");
		print("Id : $id");
		print("Salary : $sal");
	}
}

void main(){
	
	Employee obj = new Employee();
	print("Enter Employee Name: ");
	obj.name = stdin.readLineSync();
		
	print("Enter ID");
	obj.id = int.parse(stdin.readLineSync()!);
	
	print("Enter sal");
	obj.sal = double.parse(stdin.readLineSync()!);

	obj.EmpInfo();

}
