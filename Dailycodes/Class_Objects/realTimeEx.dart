
import 'dart:io';

class ProgramingLang {

	String? lName;
	int? year;
	String? fName;

	void Info (){
		print("\n-------Information--------");
		print("Name : $lName");
		print("Founder Name : $fName");
		print("Found Year : $year");
	}
}
void main(){

	ProgramingLang obj = new ProgramingLang();

	print("*************Enter Information*************");
	print("Enter Language Name:");
	obj.lName = stdin.readLineSync();

	print("Enter Founder Name");
	obj.fName = stdin.readLineSync();

	print("Enter year");
	obj.year = int.parse(stdin.readLineSync()!);

	obj.Info();
}
