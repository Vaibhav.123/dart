

import 'dart:io';

class Player {
	
	String? pName = "Virat";
	int? jerNo = 18;
	
	void PlayerInfo(){

		print("Name : $pName");
		print("JerNo : $jerNo");
	}
}

void main(){

	Player obj = new Player();

	obj.PlayerInfo();
	
	print("Change Data");
	print("Enter player name");
	obj.pName = stdin.readLineSync();
	print("Enter JerNo");
	obj.jerNo = int.parse(stdin.readLineSync()!);
	print("");
	obj.PlayerInfo();
}
