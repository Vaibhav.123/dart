// in the below code if there is no return line then the type is void
// but there is return statement which return interger so the return type is int


void main(){

	var add = (int a, int b) {
		
		return a+b;
	};
	int x = 10;
	print(add(10,20));     // 30
		
	print(x.runtimeType);      // int
	print(add.runtimeType);   // (int, int) => int
}
