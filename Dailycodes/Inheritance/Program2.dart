
class Parent {
	
	int x = 10;
	String str1 = "name";
	void parentMethod(){
		print(x);
		print(str1);
	}
}

class Child extends Parent {

	int y = 20;
	String str2 = "data";
	void childMethod(){
		print(y);
		print(str2);
	}
}

void main(){
	
	Parent obj = new Parent();
	print(obj.x);  		// Error: The getter 'y' isn't defined for the class 'Parent'.
	print(obj.str1);  	// Error: The getter 'str2' isn't defined for the class 'Parent'.
	obj.parentMethod();  	// Error: The method 'childMethod' isn't defined for the class 'Parent'.
	
	Child obj2 = new Child();
	print(obj2.y);
	print(obj2.str2);
	obj2.childMethod();
	
	print(obj2.x);
	print(obj2.str1);
	obj2.parentMethod();

}
/* OP : 
10
name
10
name
20
data
20
data
10
name
10
name
*/
