// using getter method 

class Parent {
	
	int x = 10;
	String str1 = "name";
	get getX => x;
	get getStr1 => str1;
}

class Child extends Parent {

	int y = 20;
	String str2 = "data";
	get getY => y;
	get getStr2 => str2;
}

void main(){
	
	Child obj = new Child();
	print(obj.getX);
	print(obj.getStr1);
	print(obj.getY);
	print(obj.getStr2);
}
/* OP :
10
name
20
data
*/
