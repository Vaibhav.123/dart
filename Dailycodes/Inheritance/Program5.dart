// all things are come to child class except parent constructor

class Parent {
	
	int x = 10;
	String str1 = "name";
	Parent(){
		print("Parent Constructor");
	}
	void parentMethod(){
		print(x);
		print(str1);
	}
}

class Child extends Parent {

	Child(){
		print("Child Constructor");
	}
	int x = 20;
	String str1 = "data";
	void childMethod(){
		print(x);
		print(str1);
	}
}

void main(){
	
	Parent obj1 = new Parent();	
	Child obj2 = new Child();
	print(obj2.x);
	print(obj2.str1);
	obj2.parentMethod();
}
/*
Parent Constructor
Parent Constructor
Child Constructor
20
data
20
data
*/
