// Queue cha internal implementation ListQueue ahe

import 'dart:collection';

void main(){

	var compData = Queue();
	
	compData.add("Microsoft");
	compData.add("Amazon");
	compData.add("Google");

	print(compData);     // {Microsoft, Amazon, Google}

	print(compData.runtimeType);     // ListQueue<dynamic>

	compData.addFirst("Apple"); 
	compData.addLast("Veritas");
	print(compData);     // {Apple,Microsoft, Amazon, Google,Veritas}

	compData.removeFirst();
	compData.removeLast();

	print(compData);      // {Microsoft, Amazon, Google}

}
