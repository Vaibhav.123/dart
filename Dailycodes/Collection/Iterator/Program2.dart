

void main(){

	var players = ["Rohit","Shubhman","Virat","KLRahul"];

	print(players.runtimeType);      // List<String>

	print(players.hashCode);      // 2345678987

	print(players.length);        // 4

	print(players.first);        // Rohit

	print(players.last);         // KLRahul

	print(players.iterator);     // Instance of 'ListIterator<String>'
}
