

void main(){

	var players = ["Rohit","Shubhman","Virat","KLRahul"];

	print(players);   //  [Rohit, Shubhman, Virat, KLRahul]

	var itr = players.iterator;

	print(players[0].runtimeType);    // String

	print(itr.current.runtimeType);     // String
	// both of the type are String

	
	print(itr.current);     // error : unhandled exception null type

	while(itr.moveNext()){
		if(itr.current == "Virat")
			itr.current = "Virat Kohli";     // error: cant change iterator type 
	}

	print(players);    // [Rohit, Shubhman, Virat Kohli, KLRahul]
}
