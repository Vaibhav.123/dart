

void main(){

	var players = ["Rohit","Shubhman","Virat","KLRahul"];
	
	// print
	print(players);    //  [Rohit, Shubhman, Virat, KLRahul]

	// for
	for(var data in players){
		print(data);
	}

	// forEach
	players.forEach(print);
}
/*
// print 
[Rohit, Shubhman, Virat, KLRahul]
// for
Rohit
Shubhman
Virat
KLRahul
// forEach
Rohit
Shubhman
Virat
KLRahul

*/
