// print the days in month particular

import 'dart:io';

void main(){

	print("Enter month ");
	int num = int.parse(stdin.readLineSync()!);

	if(num == 1)
		print("Jan has 31 days");
	else if(num == 2)
		print("Feb has 30 days");
	else if(num == 3)
		print("March has 31 days");
	else if(num == 4)
		print("April has 30 days");
	else if(num == 5)
		print("May has 31 days");
	else if(num == 6)
		print("June has 30 days");
	else if(num == 7)
		print("July has 31 days");
	else if(num == 8)
		print("Aug has 31 days");
	else if(num == 9)
		print("Sep has 30 days");
	else if(num == 10)
		print("Oct has 31 days");
	else if(num == 11)
		print("Nov has 30 days");
	else if(num == 12)
		print("Dec has 31 days");
	else
		print("Invalid Month");
}

