// electricity bill calculation 

import 'dart:io';

void main(){

	print("Enter units");

	int num = int.parse(stdin.readLineSync()!);

	if(num < 90)
		print("No charge");
	else if(num <= 180)
		print(num*6);
	else if(num <= 250)
		print(num*10);
	else
		print(num*15);
}
