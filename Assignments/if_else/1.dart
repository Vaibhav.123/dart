// num is even or odd

import 'dart:io';

void main(){

	print("Enter number:");
	
	int? num = int.parse(stdin.readLineSync()!);
	if(num%2 == 0)
		print("$num is an Even Number");
	else
		print("$num is Odd Number");
}
