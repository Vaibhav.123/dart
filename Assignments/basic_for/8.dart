
// print table of any number in reverse order

import 'dart:io';

void main(){
	
	print("Enter Number:");
	int num = int.parse(stdin.readLineSync()!);

	for(int i=10; i>=1; i--){
		
		print(num*i);
	}
}
