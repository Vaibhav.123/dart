

import 'dart:io';

void main(){

	print("Enter number ");
	int x = int.parse(stdin.readLineSync()!);
	
	int rev = 0;
	int temp = x;
	
	while(x != 0){
		
		int rem = x%10;
		rev = rev*10 + rem;
		x = (x/10).floor();
	}
	if(temp == rev)
		print("$temp is palindrome");
	else
		print("$temp is not palindrome");
}
