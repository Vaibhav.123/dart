
// calculate the factorial of number 

import 'dart:io';

void main(){

	print("Enter number: ");
	int num = int.parse(stdin.readLineSync()!);
	int fac = 1;
	while(num != 0){
		fac = fac*num;
		num--;
	}
	print("factorial = $fac");
}
