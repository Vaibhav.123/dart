
// take a number from user 
// and check if number is even then print that number in reverse order 
// if number is odd then print that number in reverse order by diff two

// to covert double to int round(), toInt(),ceil(), floor() 
import 'dart:io';

void main(){

	print("Enter number: ");
	int x = int.parse(stdin.readLineSync()!);

	if(x%2 == 0){
		while(x != 0){
			print(x--);
		}
	}else{ 
		while(x != -1){
			print(x);
			x = x-2;
		}
	}
}

