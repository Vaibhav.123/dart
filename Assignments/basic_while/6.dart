
// print the sum of all even numbers and multiplication of odd numbers
// between the range
// to covert double to int round(), toInt(),ceil(), floor() 
import 'dart:io';

void main(){

	print("Enter number: ");
	int x1 = int.parse(stdin.readLineSync()!);
	int x2 = int.parse(stdin.readLineSync()!);
	int sum = 0;
	int pro = 1;

	while(x1 <= x2){
		if(x1%2 == 0)
			sum = sum+x1;
		else 
			pro = pro*x1;
		x1++;
	}
	print("Sum of even numbers = $sum");
	print("Mul of odd numbers = $pro");
}
