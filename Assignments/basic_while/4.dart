
// count the odd digits of given number 
// to covert double to int round(), toInt(),ceil(), floor() 
import 'dart:io';

void main(){

	print("Enter number: ");
	int x = int.parse(stdin.readLineSync()!);
	int count = 0;

	while(x != 0){
		int rem = x%10;
		if(rem%2 == 1)
			count++;

		x = (x/10).floor();	
	}
	print("Odd count $count");
}
